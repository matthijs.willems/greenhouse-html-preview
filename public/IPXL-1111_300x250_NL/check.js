

var returnPositive = function returnPositive() {
  return true;
};

var returnNegative = function returnNegative() {
  return false;
};

var _navigator = navigator;


var userAgent = _navigator.userAgent;
var Check = {
  retina: function retina() {
    return window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches) || window.devicePixelRatio && window.devicePixelRatio >= 2;
  },
  desktop: function desktop() {
    return !this.mobile();
  },
  mobile: function mobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent);
  },
  osx: function osx() {
    // Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2)
    // AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36
    return /OS X/i.test(userAgent);
  },
  windows: function windows() {
    // Mozilla/5.0 (Windows NT 6.1; WOW64)
    // AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36
    return /Windows/i.test(userAgent);
  },
  ios: function ios() {
    return /iPhone|iPad|iPod/i.test(userAgent);
  },
  android: function android() {
    return /Android/i.test(userAgent);
  },
  ie: function ie() {
    return /MSIE|Trident/i.test(userAgent);
  },
  ie9: function ie9() {
    // Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; SLCC2;
    // .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)
    return /MSIE 9\./i.test(userAgent);
  },
  ie10: function ie10() {
    // Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1;
    // Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729;
    // Media Center PC 6.0; .NET4.0C)
    return /MSIE 10\./i.test(userAgent);
  },
  ie11: function ie11() {
    // Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2;
    // .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729;
    // Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3; rv:11.0) like Gecko
    return /Trident\//i.test(userAgent) && !/MSIE/i.test(userAgent);
  },
  edge: function edge() {
    // 5.0 (Windows NT 10.0; Win64; x64)
    // AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240
    return /Edge\//i.test(window.navigator.userAgent);
  },
  chrome: function chrome() {
    // Mozilla/5.0 (Windows NT 6.1; WOW64)
    // AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36
    return /Chrome\//i.test(userAgent) && !/Edge\//i.test(userAgent);
  },
  firefox: function firefox() {
    // Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0
    return /Firefox\//i.test(userAgent);
  },
  safari: function safari() {
    // Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4)
    // AppleWebKit/536.30.1 (KHTML, like Gecko) Version/6.0.5 Safari/536.30.1
    return /Safari\//i.test(userAgent) && !/Chrome\//i.test(userAgent) && !/Edge\//i.test(userAgent);
  }
};
