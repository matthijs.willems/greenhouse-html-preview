/////////////////
///  SETTINGS ///
/////////////////
var settings = {
	// Fill out the countdown date here, generate via: https://timestampgenerator.com/1614985200/+01:00
	// Copy your preferred Common Date Format, paste below between the quotes "--"
	countdownDate: "Mon, 16 Mar 2021 12:00:00 +0100",
	// The clock is used as a percentage pie, it will calculate the remaining percentage. Default = 24;
	totalHours: 24,
	cta: "BESTEL NU",
	pancakeCopy: "<big>21%</big><big2>KORTING*</big2><small>OP ALLES</small>",
	timerCopy: "<smaller>EINDIGT OVER</smaller><div class='countdown'>%countdown%</div>",
	timerOverCopy: "<big2>ACTIE<br>VOORBIJ</big2>",
	labelCopy: "ALLEEN VANDAAG",
	disclaimerCopy: "*Enkel vandaag geldig",
};

/////////////////
///    VARS   ///
/////////////////

var creativeWidth = $("#creative_container").outerWidth();
var creativeHeight = $("#creative_container").outerWidth();

var loopstopTimer = setTimeout(onLoopstop, 14500);
var loopstop = false;
var tickerBoolean = true;
var countdownInterval, countdownText;

var remainingTime = getRemainingTime(settings.countdownDate);
var hoursLeft = remainingTime.hours < 1 ? 360 - 6 : 360 - (remainingTime.hours / settings.totalHours) * 360;
var secondsLeft = remainingTime.seconds < 1 ? 360 - 6 : 360 - (remainingTime.seconds / 60) * 360;
var fullHoursLeft =
	remainingTime.fullHours < 1
		? 360 - 6
		: remainingTime.fullHours > settings.totalHours
		? 360 - (settings.totalHours / settings.totalHours) * 360
		: 360 - (remainingTime.fullHours / settings.totalHours) * 360;

var clickUrl = "https://www.iciparisxl.nl/";

var dynamicPath = {
	start: 0,
	end: 0,
	cx: 150,
	cy: 150,
	r: 150,
};

////////////////
/// SETTINGS ///
////////////////

$(".ctaContainer").html(settings.cta).append(ctaArrow);
$(".pancake").html(settings.pancakeCopy);
$(".label").html(settings.labelCopy);
$(".disclaimer").html(settings.disclaimerCopy);
updateCountdown($(".timer"), settings.timerCopy);

//////////////////
/// ANIMATIONS ///
//////////////////

var clockAnimationDuration = 4;
var tickerAnimation = gsap.timeline();

// Product animation
var productAnimation = gsap.timeline().staggerFrom(
	[
		".productImage:nth-child(5)",
		".productImage:nth-child(2)",
		".productImage:nth-child(4)",
		".productImage:nth-child(3)",
		".productImage:nth-child(1)",
		".productImage:nth-child(6)",
	],
	0.5,
	{
		scale: 0.5,
		autoAlpha: 0,
		ease: "elastic.out.config(0.5, 1)",
	},
	0.15,
	0
);

// Clock animation
var clockAnimation = gsap
	.timeline()
	.to(dynamicPath, 2, { end: 360, ease: Power2.easeInOut, onUpdate: updatePath }, 0)
	.to(".smallPointer", 2, { rotation: 360, ease: Power2.easeInOut }, 0)
	.to(dynamicPath, 1, { end: fullHoursLeft, ease: Power2.easeInOut, onUpdate: updatePath }, 2)
	.to(".smallPointer", 1, { rotation: fullHoursLeft, ease: Power2.easeInOut }, 2)
	.to(".bigPointer", 2, { rotation: 360 * 6, ease: Power2.easeInOut }, 0)
	.to(".bigPointer", 1, { rotation: secondsLeft + 6 * (clockAnimationDuration - 1), ease: Power2.easeInOut }, 2)
	.add(productAnimation, 2)
	.call(playTickerAnimation, ["start"]);

// Background animation
var backgroundAnimation = gsap
	.timeline({
		repeat: -1,
	})
	.to("#creative_container", 5, {
		backgroundPosition: "-100% 100%",
		ease: "none",
	});

// Intro animation
var introAnimation = gsap
	.timeline()
	.fromTo(".beautyLogo", 0.5, { transformOrigin: "left top", autoAlpha: 0, scale: 0.2 }, { autoAlpha: 1, scale: 0.6, ease: "expo.out" })
	.fromTo(".pancakeContainer", 0.5, {scale: 0.8, autoAlpha: 0}, {scale: 1.35, rotation: 0, autoAlpha: 1, ease: "expo.out"}, 0)
	.to(".pancakeContainer", 0.25, {y: -64}, 1.5)
	.fromTo(
		".clockContainer",
		2,
		{ y: $("#header").outerHeight() / 2 + 20, scale: 0.65, autoAlpha: 0 },
		{ scale: 1, autoAlpha: 1, ease: "elastic.out(1, 0.75)" }, 1.5
	);

// End animation
var endAnimation = gsap
	.timeline()
	.to(".beautyLogo", 1.5, { transformOrigin: "left top", scale: 1, ease: "elastic.inOut(1.5, 0.75)" }, 0)
	.to(".clockContainer", 0.75, { scale: 0.75, y: "-=0", x: "+=54", ease: "expo.inOut" }, 0.3)
	.to(".productContainer", 0.75, { scale: 0.9, y: "-=7", x: "+=50", ease: "expo.inOut" }, 0.3)
	.to(".pancakeContainer", 0.75, { scale: 1.35, x: "+=56", y: "+=10", ease: "expo.inOut" }, 0.4)
	.from(".timerContainer", 1, { autoAlpha: 0, y: creativeHeight / 4, ease: "expo.inOut" }, "-=0.75")
	.from(".timerContainer > .label", 1.5, { rotation: 0, ease: "elastic.out" })
	.staggerFrom(".ctaContainer, .disclaimer ", 1, { y: creativeHeight / 4, ease: "expo.inOut" }, 0.15, "-=2.5")
	.to(".ctaContainer", 0.1, { y: -5 }, "+=1")
	.to(".ctaContainer", 1, { y: 0, ease: "elastic.out(1, 0.3)" });


// Main timeline animation
var mainTimeline = gsap.timeline({paused: true})
	.to(".click", 0.25, { autoAlpha: 1, ease: "expo.out" })
	.add(introAnimation, 0)
	.add(clockAnimation, 2)
	.add(endAnimation, '-=0.5');

///////////////////
/// INTERACTION ///
///////////////////

$("#creative_container").hover(onUserEnter, onUserLeave);

$("#click").on("click", clickout);

///////////////////
/// FUNCTIONS   ///
///////////////////

function onLoopstop() {
	loopstop = true;
	backgroundAnimation.pause();
	tickerBoolean = false;
	clearInterval(countdownInterval);
	countdownInterval = undefined;
}

function onUserEnter(e) {
	if (loopstop) {
		backgroundAnimation.play();
		tickerBoolean = true;
		playTickerAnimation();
	}
	$(".ctaContainer, .ctaContainer > svg").addClass("hover");
}

function onUserLeave(e) {
	$(".ctaContainer, .ctaContainer > svg").removeClass("hover");
	if (loopstop) {
		backgroundAnimation.pause();
		tickerBoolean = false;
	}
}

function updateCountdown(element, text) {
	// If the count down is finished, write some text
	if (remainingTime.minutes < 1) {
		countdownText = settings.timerOverCopy;
	} else {
		countdownText = text.replace(
			"%countdown%",
			"<big>" + remainingTime.fullHours + "</big><small>u</small><big>" + remainingTime.minutes + "</big><small>m</small>"
		);
	}
	element.html(countdownText);
}

function clickout() {
	window.open(window.clickTag);
}

function getRemainingTime(date) {
	var countdownDate = new Date(date).getTime();
	var now = new Date().getTime();
	var distance = countdownDate - now;

	return {
		"hours": Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
		"minutes": Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
		"seconds": Math.floor((distance % (1000 * 60)) / 1000),
		"fullHours": Math.floor(distance / (1000 * 60 * 60)),
	};
}

function updatePath() {
	document
		.querySelector("#dynamicPath")
		.setAttribute("d", getPath(dynamicPath.cx, dynamicPath.cy, dynamicPath.r, dynamicPath.start, dynamicPath.end));
}

function getPath(cx, cy, r, a1, a2) {
	var delta = a2 - a1;

	if (delta === 360) {
		return "M " + (cx - r) + " " + cy + " a " + r + " " + r + " 0 1 0 " + r * 2 + " 0 a " + r + " " + r + " 0 1 0 " + -r * 2 + " 0z";
	}

	var largeArc = delta > 180 ? 1 : 0;

	a1 = a1 * (Math.PI / 180) - Math.PI / 2;
	a2 = a2 * (Math.PI / 180) - Math.PI / 2;

	var x1 = cx + r * Math.cos(a2);
	var y1 = cy + r * Math.sin(a2);

	var x2 = cx + r * Math.cos(a1);
	var y2 = cy + r * Math.sin(a1);

	return "M " + x1 + " " + y1 + " A " + r + " " + r + " 0 " + largeArc + " 0 " + x2 + " " + y2 + " L " + cx + " " + cy + "z";
}

function playTickerAnimation(start) {
	if (start) {
		secondsLeft = secondsLeft + 6 * clockAnimationDuration;
	} else {
		secondsLeft = secondsLeft + 6;
	}

	if (!tickerBoolean || tickerAnimation.isActive()) return;

	tickerAnimation.to(".bigPointer", 1, {
		rotation: secondsLeft,
		ease: "steps(1)",
		onComplete: playTickerAnimation,
	});
}

// Wait for fonts to load

WebFont.load({
	custom: {
		families: ['Gotham Bold', 'Gotham Book', 'Gotham Black']
	},
	timeout: 2000,
	active: function() {
		mainTimeline.play();
	 }
 }
);
